use bobr::raster::{IntPoint, Rasterizer};
use bobr::Result;
use std::path::Path;
use std::io::BufWriter;
use std::io::Write;
use std::fs::File;
use std::fs;

struct TestImage {
    name: String,
    polygon: Vec<IntPoint>,
}

fn main() -> Result<()> {
    const SIZE: i32 = 16;
    println!("Generating test images ({}x{})", SIZE, SIZE);
    let mut test_images: Vec<TestImage> = vec![];
    let mut add_polygon_image = |name: &str, polygon: Vec<IntPoint>| {
        test_images.push(TestImage {
            name: name.to_string(),
            polygon,
        })
    };
    add_polygon_image("fill", vec![(0, 0), (0, SIZE), (SIZE, SIZE), (SIZE, 0)]);
    add_polygon_image("fill_with_inset", vec![(1, 1), (1, SIZE - 1), (SIZE - 1, SIZE - 1), (SIZE - 1, 1)]);
    add_polygon_image("fill_L", vec![(0, 0), (0, SIZE), (SIZE, SIZE), (SIZE, SIZE * 3 / 4), (SIZE / 4, SIZE * 3 / 4), (SIZE / 4, 0)]);
    add_polygon_image("diamond", vec![(0,SIZE/2), (SIZE/2,0), (SIZE, SIZE/2), (SIZE/2, SIZE)]);
    let mut images_js = BufWriter::new(File::create("images/images.js")?);
    writeln!(images_js, "images = [];")?;
    for case in test_images {
        println!("\tGenerating {}.pgm", case.name);
        let mut rasterizer = Rasterizer::new(SIZE as u32, SIZE as u32);
        rasterizer.draw_polygon(case.polygon.iter());
        let pgm_path = Path::new(&format!("images/{}.pgm", case.name)).to_path_buf();
        rasterizer.save_as_pgm(&pgm_path)?;
        let pgm = fs::read_to_string(&pgm_path)?;
        writeln!(images_js, "images.push({{name: \"{}\", pgm: `{}`}});", case.name, pgm)?;

        println!("\tGenerated {}.pgm", case.name);
    }
    println!("Done generating test images ({}x{})", SIZE, SIZE);
    Ok(())
}