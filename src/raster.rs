use std::path::Path;
use crate::Result;
use std::fs::File;
use std::io::BufWriter;
use std::io::Write;
use std::mem::swap;

pub struct Rasterizer {
    width: u32,
    height: u32,
    buffer: Vec<i8>,
}

pub type IntPoint = (i32, i32);

impl Rasterizer {
    pub fn new(width: u32, height: u32) -> Self {
        Self {
            width,
            height,
            buffer: vec![0; (width * height) as usize],
        }
    }

    pub fn draw_polygon<'a, I: Iterator<Item=&'a IntPoint>>(&mut self, mut points: I) {
        let a = points.next().expect("at least one element");
        let mut a_st = (a.0 * 16, a.1 * 16);
        let start_st = a_st.clone();
        while let Some(b) = points.next() {
            let b_st = (b.0 * 16, b.1 * 16);
            self.line_st(&a_st, &b_st);
            a_st = b_st;
        }
        self.line_st(&a_st, &start_st);
    }

    fn line_st(&mut self, a: &IntPoint, b: &IntPoint) {
        let mut starty = a.1;
        let mut endy = b.1;
        if starty == endy {
            // same height, skip
            return;
        }
        let mut signum = 1i32;
        let mut startx = a.0;
        let mut endx = b.0;
        if startx > endx {
            swap(&mut starty, &mut endy);
            swap(&mut startx, &mut endx);
        }
        let delta_x = startx - endx;
        let delta_y = starty - endy;
        let mut yswapped = false;
        let mut yinc = 16i32;
        let mut maxy = endy;
        let mut miny = starty;
        if starty > endy {
            yswapped = true;
            swap(&mut miny, &mut maxy);
            maxy = starty;
            yinc = -yinc;
            signum = -signum;
        }

        let mut y = starty;
        loop {
            let mut upper_y = y;
            let mut lower_y = y+16;
            if yswapped {
                upper_y = y-16;
                lower_y = y;
            }
            if lower_y > maxy || upper_y < miny {
                break;
            }
            // compute trapezoid coverage of pixel
            // coverage of all pixels in row should sum to 127 to maintain invarians
            let upper_x = startx+(upper_y - starty)*delta_x/delta_y;
            let lower_x = startx+(lower_y - starty)*delta_x/delta_y;

            // local x,y
            let small_x = upper_x.min(lower_x);
            let mut pixel_x = small_x / 16;
            let pixel_y = upper_y / 16;
            // sample coverage
            let mut alpha_remaining = 127;
            loop {
                let mut samples = 0;
                for lx in pixel_x*16..pixel_x*16+16 {
                    for ly in pixel_y*16..pixel_y*16+16 {
                        if signum*(delta_x * (ly - endy) - delta_y * (lx - endx)) >= 0 {
                            samples += 1;
                        }
                    }
                }
                let alpha = (samples / 2).min(alpha_remaining);
                if pixel_x < self.width as i32 {
                    self.buffer[(pixel_y * self.width as i32 + pixel_x) as usize] += (signum * alpha) as i8;
                }
                alpha_remaining -= alpha;
                pixel_x+=1;
                let refx = pixel_x * 16 - 16;
                if alpha_remaining <= 0 || (refx > upper_x && refx > lower_x) {
                    break;
                }
            }
            y += yinc;
            /*
            for h in starty..endy {
                if x < self.width {
                    self.buffer[(h * self.width + x) as usize] = signum;
                }
                x = (x as i32 + dx) as u32;
            }
             */
        }
    }

    pub fn save_as_pgm(&self, path: &Path) -> Result<()> {
        let mut image = Vec::with_capacity((self.width * self.height) as usize);
        for h in 0..self.height {
            let mut acc = 0i8;
            for w in 0..self.width {
                let add = self.buffer[(h * self.width + w) as usize];
                acc += add;
                let alpha = (acc.abs() as u32 * 2) as u8;
                image.push(alpha);
            }
        }

        let mut file = BufWriter::new(File::create(&path)?);

        write!(file, "P2 {} {} 255\n", self.width, self.height)?;
        for h in 0..self.height {
            for w in 0..self.width {
                let alpha = image[(h * self.width + w) as usize];
                write!(file, "{} ", 255-alpha)?;
            }
            write!(file, "\n")?;
        }
        Ok(())
    }
}


#[cfg(test)]
mod tests {
    use crate::raster::Rasterizer;
    use std::path::Path;
    use crate::Result;

    const SIZE: u32 = 8;


    #[test]
    fn draw_polygon() -> Result<()> {
        let mut rasterizer = Rasterizer::new(SIZE, SIZE);
        rasterizer.draw_polygon([(0, 0), (0, SIZE), (SIZE, SIZE), (SIZE, 0)].iter());
        rasterizer.save_as_pgm(Path::new("images/test.pgm"))
    }
}
